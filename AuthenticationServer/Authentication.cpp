#pragma comment(lib, "Ws2_32.lib")
#include "Authentication.h"

std::string getUsername;
std::string getCreationDate;
int getUsersId;

Authentication::Authentication()
{

	size = 512;
	index = 0;
	recvBuffer = new Buffer(size);
	sendMessages = true;
	recvbuf[DEFAULT_BUFFER_LENGTH];
	recvbuflen = DEFAULT_BUFFER_LENGTH;
	result = 0;

	alphabets ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	stringLength = 52;
	serverName = "";
	

	if (!dbHelper.ConnectToDatabase("127.0.0.1:3306", "root", "password", "player"))
	{
		std::cout << "Failed to connect to the database!" << std::endl;
		return;
	}
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;



	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed: %d\n", iResult);
		return;
	}

	// Socket()
	ListenServerSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (ListenServerSocket == INVALID_SOCKET) {
		printf("socket() failed with error %d\n", WSAGetLastError());
		WSACleanup();
		return;
	}

	// Bind()
	iResult = getaddrinfo(NULL, DEFAULT_AUTHENTICATION_PORT, &hints, &result);
	iResult = bind(ListenServerSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind() failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenServerSocket);
		WSACleanup();
		return;
	}

	// Listen()
	if (listen(ListenServerSocket, 5)) {
		printf("listen() failed with error: %d\n", WSAGetLastError());
		closesocket(ListenServerSocket);
		WSACleanup();
		return;
	}
	return;
}

Authentication::~Authentication()
{
	for (int index = 0; index != threads.size(); index++)
	{
		this->threads[index]->join();
	}

	closesocket(ListenServerSocket);
	WSACleanup();
	return;
}


void Authentication::acceptServer()
{
	int clientCounter = 0;
	myServerSocket tempSocket;

	do {
		SOCKET tempServerSocket;
		// Accept()
		tempServerSocket = accept(ListenServerSocket, NULL, NULL);
		int bp = 0;
		if (tempServerSocket == INVALID_SOCKET) {
			printf("accept() failed with error: %d\n", WSAGetLastError());
			closesocket(ListenServerSocket);
			WSACleanup();
			//return;
		}

		tempSocket.serverSocket = tempServerSocket;
		tempSocket.serverName = "Server " + std::to_string(clientCounter);
		tempSocket.id = clientCounter;
		clientCounter++;

		//this->serverSockets.push_back(tempServerSocket);		
		this->serverSockets.push_back(tempSocket);
		std::thread* serverListenThread = (new std::thread(&Authentication::listenToServer, this, tempSocket));

		//this->threads.push_back(clientListenThread);
		std::cout << "Server " << tempSocket.serverName << " Connected - New Thread Started" << std::endl;
	} while (sendMessages);

}// end acceptServer



void Authentication::listenToServer(myServerSocket tempSocket)
{
	bool connected = true;
	//Web::CreateAccountWeb deserialized_createAccount;

	do {
		iResult = recv(tempSocket.serverSocket, recvBuffer->vecBuffer.data(), recvBuffer->vecBuffer.size(), 0);
		
		//std::cout << "Received Serialized Message : " << getMessageFromServer << std::endl;
		if (iResult == SOCKET_ERROR) {
			std::cout << "\nServer " << tempSocket.serverName << " has exited the application" << std::endl;
			closesocket(tempSocket.serverSocket);

			for (int i = 0; i != serverSockets.size(); i++)
			{
				if (serverSockets[i].serverSocket == tempSocket.serverSocket)
				{
					this->serverSockets.erase(serverSockets.begin() + i);
					connected = false;
					break;
				}
			}

		}
		if (iResult > 0) {
			std::string messFromAuth;

			int typeOfProtocol = recvBuffer->ReadInt32BE(0);
			int numOfBytesToRecv = recvBuffer->ReadInt32BE(4);
			std::string receivedMessage = recvBuffer->ReadString(8, numOfBytesToRecv);

			std::cout << "Received from server : " << receivedMessage << std::endl;

			
			std::string username;
			std::string password;

			//888
			if (typeOfProtocol == 4)
			{
				username = receivedMessage.substr(0, receivedMessage.find(" "));
				password =  receivedMessage.substr(receivedMessage.find_last_of(" \t") + 1);

				messFromAuth = registerNewUser(username, password);
			}
			else if (typeOfProtocol == 5)
			{
				username = receivedMessage.substr(0, receivedMessage.find(" "));
				password = receivedMessage.substr(receivedMessage.find_last_of(" \t") + 1);

				messFromAuth = loginClient(username, password);
			}
			else if (typeOfProtocol == 7)
			{
				username = receivedMessage.substr(receivedMessage.find_last_of(" \t") + 1);

				messFromAuth = logout(username);
			}

			//std::string messFromAuth = registerNewUser(username, password);

			//std::cout<< "Message from Auth Server : " << messFromAuth << std::end;

			// SEND TEST MESSAGE TO SERVER
			Buffer tempBuffer(DEFAULT_BUFFER_LENGTH);

			//std::string sendToServer = "test";
			tempBuffer.WriteInt32BE(4, messFromAuth.size());					 // send message to yourself that you have joined the room 
			//tempBuffer.WriteString(strlen(messFromAuth.c_str()), messFromAuth);		 // send message to yourself that you have joined the room 
			tempBuffer.WriteString(8, messFromAuth);		 // send message to yourself that you have joined the room 


			iResult = send(tempSocket.serverSocket, tempBuffer.vecBuffer.data(), tempBuffer.vecBuffer.size(), 0);
			int breakpoint = 0;
			if (iResult == SOCKET_ERROR)
			{
				printf("recv failed with error: %d\n", WSAGetLastError());
				closesocket(tempSocket.serverSocket);
				WSACleanup();
			}

			


		}

		

	} while (sendMessages && connected);
}




 /*
 Function Name: IntToString
 Return Type : String
 Parameters : int
 Purpose : Converts any integer value to string
 */
std::string Authentication::IntToString(int number)
{
	std::ostringstream oss;

	oss << number;

	return oss.str();
}

/*
Function Name: getRandomCharacter
Return Type : char
Parameters : NA
Purpose : returns random character necessary to form salt
*/
char Authentication::getRandomCharacter()
{
	return alphabets[rand() % (alphabets.size()-1)];
}

/*
Function Name: generateSalt
Return Type : String
Parameters : NA
Purpose : generates salt value of length 10
*/
std::string Authentication::generateSalt()
{
	srand(time(0));
	std::string salt;
	for (unsigned int i = 0; i < 10; ++i)
	{
		salt += getRandomCharacter();

	}
	return salt;
}


void Authentication::ExampleUpdate()
{
	int numUpdates = dbHelper.ExecuteUpdate("UPDATE contact SET address = 'authentication' WHERE id = 1");

}

/*
Function Name: addToUser
Return Type : NA
Parameters : NA
Purpose : Creates new user by adding into a user table of database
*/
void Authentication::addToUser()
{
	dbHelper.Execute("INSERT INTO accounts(last_login, creation_date) VALUES(now(),now())");					//Add entry to user table
}


/*
Function Name: hashOfPassword
Return Type : String
Parameters : const char*
Purpose : Generates hash of password
*/
std::string Authentication::hashOfPassword(std::string password)
{

	unsigned char digest[SHA256_DIGEST_LENGTH] = "";
	SHA256_Init(&ctx);
	SHA256_Update(&ctx, password.c_str(), password.size());
	SHA256_Final(digest, &ctx);

	char hash[SHA256_DIGEST_LENGTH * 2 + 1] = "";
	for (int i = 0; i < SHA256_DIGEST_LENGTH; i++)
	{
		sprintf(&(hash[i * 2]), "%02x", (unsigned int)(digest[i]));
	}
	return hash;
}

/*
Function Name: registerNewUser
Return Type : String
Parameters : string, string
Purpose : Checks if the user already exists or not.
If not Registers new user by updating user table as well as player table with supplied username and password
Generates salt, combines it with password and then generates the hashed password
*/
std::string Authentication::registerNewUser(std::string username, std::string password)
{
	
	std::string salt, messageToSend, saltPass, passHash;

	salt = generateSalt();
	saltPass = salt + password + "\0";
	passHash = hashOfPassword(saltPass);


	std::string sqlQuery = "SELECT * FROM accounts WHERE username = '" + username + "'";
	sql::ResultSet* resultUsername = dbHelper.ExecuteQuery(sqlQuery);
	if (resultUsername->rowsCount() > 0)
	{
		messageToSend = "ACCOUNT_ALREADY_EXISTS";
		return messageToSend;
	}
	if (password.size() < 5)
	{
		messageToSend = "INVALID_PASSWORD";
		return messageToSend;
	}
	//TODO: get newly created userid from user table here
	//addToUser();
	std::string getLastItem = "SELECT id FROM accounts ORDER BY id DESC LIMIT 1";
	sql::ResultSet* result = dbHelper.ExecuteQuery(getLastItem);
	int newUserId;
	if (result->rowsCount() > 0)
	{
		while (result->next())
		{
			newUserId = result->getInt(1);

		}

	}

	sqlQuery = "INSERT INTO accounts(username, salt, password) VALUES('"
		+ username + "','" + salt + "','" + passHash + "')";

	dbHelper.Execute(sqlQuery);
	messageToSend = "Account Creation Successful";
	return messageToSend;
}


/*
Function Name: authenticateClient
Return Type : string
Parameters : string, string
Purpose : Authenticates user based on username and password.
Validates username first. If username is valid, checks for password by generating hash after combining with salt for that username.
*/
std::string Authentication::loginClient(std::string username, std::string password)
{
	int found = 0;
	std::string saltPass, hashPass, messageToSend;

	std::string getUsername;
	std::string getSalt;
	std::string getHashPassword;
	int getUserId;
	int getCurrentlyLoggedInStatus;

	std::string sqlQuery = "SELECT * FROM accounts WHERE username = '" + username + "'";
	sql::ResultSet* result = dbHelper.ExecuteQuery(sqlQuery);
	if (result->rowsCount() > 0)
	{
		while (result->next())
		{
			
			getUserId = result->getInt(1); 
			getUsername = result->getString(2);
			getSalt = result->getString(3);
			getHashPassword = result->getString(4);
			getCurrentlyLoggedInStatus = result->getInt(5);

			if (getCurrentlyLoggedInStatus == 1)
			{
				messageToSend = "This user is already logged in! please use a different login! "; 
				return messageToSend;
			}

			if (username == getUsername)
			{
				found = 1;
				break;
			}
		}
	}
	if (found == 1)
	{
		saltPass = getSalt + password + "\0";
		hashPass = hashOfPassword(saltPass);
		if (hashPass == getHashPassword)
		{
			int numUpdates = dbHelper.ExecuteUpdate("UPDATE accounts SET last_login = now(), currently_logged_in = '1' WHERE id = '" + IntToString(getUserId) + "';");
			
			std::string sqlQueryForRegDate = "SELECT * FROM accounts WHERE id = '" + IntToString(getUserId) + "'";
			sql::ResultSet* resultRegDate = dbHelper.ExecuteQuery(sqlQueryForRegDate);
			if (resultRegDate->rowsCount() > 0)
			{
				while (resultRegDate->next())
				{
					getCreationDate = resultRegDate->getString(6);
					getUsername = resultRegDate->getString(2);
					getUsersId = resultRegDate->getInt(1);
				}
			}
			//messageToSend = "4";
			messageToSend = "Login Successful! Welcome " + getUsername + "!";				//Display login successful

		}
		else
		{
			messageToSend = "INVALID_PASSWORD";
		}
	}
	else
	{
		messageToSend = "INVALID_USERNAME";
	}

	//std::cout << messageToSend << std::endl;
	return messageToSend;
}


/*
Function Name: logout
Return Type : string
Parameters : string
Purpose : sets the currently logged in flag in database to 0 
*/
std::string Authentication::logout(std::string username)
{
	
	int found = 0;
	std::string saltPass, hashPass, messageToSend;

	std::string getUsername;
	std::string getSalt;
	std::string getHashPassword;
	int getUserId;

	std::string sqlQuery = "SELECT * FROM accounts WHERE username = '" + username + "'";
	sql::ResultSet* result = dbHelper.ExecuteQuery(sqlQuery);
	if (result->rowsCount() > 0)
	{
		while (result->next())
		{

			getUserId = result->getInt(1);
			getUsername = result->getString(2);
			getSalt = result->getString(3);
			getHashPassword = result->getString(4);

			if (username == getUsername)
			{
				found = 1;
				break;
			}
		}
	}
	if (found == 1)
	{
	
		
			int numUpdates = dbHelper.ExecuteUpdate("UPDATE accounts SET last_login = now(), currently_logged_in = '0' WHERE id = '" + IntToString(getUserId) + "';");

			std::string sqlQueryForRegDate = "SELECT * FROM accounts WHERE id = '" + IntToString(getUserId) + "'";
			sql::ResultSet* resultRegDate = dbHelper.ExecuteQuery(sqlQueryForRegDate);
			if (resultRegDate->rowsCount() > 0)
			{
				while (resultRegDate->next())
				{
					getCreationDate = resultRegDate->getString(6);
					getUsername = resultRegDate->getString(2);
					getUsersId = resultRegDate->getInt(1);
				}
			}
			//messageToSend = "4";
			messageToSend = "Logout Successful! Goodbye " + getUsername + "!";				//Display login successful

		
		
	}
	//std::cout << messageToSend << std::endl;
	return messageToSend;
	
}// end logout

