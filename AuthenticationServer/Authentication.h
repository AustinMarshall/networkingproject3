#ifndef _AUTHENTICATION_HG_
#define _AUTHENTICATION_HG_


#define UNICODE
#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <WinSock2.h>
#include <WS2tcpip.h>

#include "..\shared\Buffer.h"

#include <stdio.h>
#include <iostream>
#include <thread>
#include <sstream>

#include "DatabaseHelper.h"

#include <openssl\conf.h>
#include <openssl\evp.h>
#include <openssl\err.h>
#include <openssl\sha.h>

#define DEFAULT_AUTHENTICATION_PORT "4500"
#define DEFAULT_BUFFER_LENGTH 512



enum action
{
	JOIN = 0,
	LEAVE = 1,
	SEND = 2,
	RECEIVE = 3,
	LISTROOMS = 4
};


struct myServerSocket
{
	SOCKET serverSocket;
	std::string serverName;
	int id;

};

class Authentication
{
public:
	DatabaseHelper dbHelper;

	SHA256_CTX ctx;
	
	std::string alphabets;

	int stringLength;


	size_t size;
	size_t index;

	Buffer* recvBuffer;

	bool sendMessages;

	std::vector<myServerSocket> serverSockets;

	std::vector<std::thread*> threads;

	std::string serverName;

	SOCKET ListenServerSocket;
	WSADATA wsaData;
	int iResult;

	char recvbuf[DEFAULT_BUFFER_LENGTH];
	int recvbuflen = DEFAULT_BUFFER_LENGTH;

	//SOCKET ListenSocket;

	//char getStringFromClient[100];
	struct addrinfo* result;
	struct addrinfo hints;

	Authentication();
	~Authentication();
	void acceptServer();												//Accepts Clients on a thread and creates a new thread to listen

	void listenToServer(myServerSocket tempSocket);							//Keeps on listening for any incoming client

	//void sendToServer();												//Send data to a client communicating with the server

	//void broadcastMessage(myAuthSocket activeServer, std::string receivedMessage); // broadcast a message to everyone in the current room 
	std::string IntToString(int number);
	char getRandomCharacter();
	std::string generateSalt();
	void ExampleUpdate();
	void addToUser();
	std::string hashOfPassword(std::string password);
	std::string registerNewUser(std::string email, std::string password);
	std::string loginClient(std::string email, std::string password);
	std::string logout(std::string username);

};


#endif // !_AUTHENTICATION_HG_
