Course: Network Programming
Author: Austin Marshall
Date: December 21, 2017

Configuration:
Microsoft Visual Studio 2017

Project 2:


How to build:
Open ProtobufExample.sln
Select Debug as Solution Configuration and x86 as Solution Platform, then the Build Solution

Set up database: 
1) Import networkproject2.sql in MySql Workbench

Pre-requisites:
1) Navigate to AuthenticationServer\Authentication.cpp
2) Change database setting for username and password in the constructor
 
How to Run:
1) Open the Debug folder in the solution folder
2) Run the AuthenticationServer.exe
3) Open command prompt, and change directory to Debug
4) Run: Server.exe 127.0.0.1
5) Open another instance of command prompt, and change directory to Debug
6) Run: Client.exe 127.0.0.1
7) Follow the command in client
   - register email password
   - authenticate email password




Protocol Used in Project 1:
    [int]            [int]         [string]
[typeOfMessage] [lengthOfMessage] [message]
     
	 
Commands for the application:
ls 					- lists available rooms
join <roomName> 	- joins a room with specified name. If the room is not available, it creates new room
leave <roomName> 	- leaves a specified room
no command 			- Sends input text as message to the members of the room
