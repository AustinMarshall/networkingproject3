#include "Client.h"


Client::Client()
{
	// get the map names, game modes and number of players the user can choose 

	std::string filePathForTXTLoader = "../dev/assets/data.txt";

	std::ifstream data(filePathForTXTLoader.c_str());
	std::string tempString;

	 username = ""; 


	if (!data.is_open())
	{	// Didn't open file, tell user
		std::cout << "Map Names, Game modes not loaded!!!" << std::endl; 
	}

	ReadFileToToken(data, "MapNames");
	// six map names so this is hard coded 
	for (int i = 0; i < 6; i++)
	{
		data >> tempString;
		mapNames.push_back(tempString);
	}

	ReadFileToToken(data, "GameModes");
	// six map names so this is hard coded 
	for (int i = 0; i < 4; i++)
	{
		data >> tempString;
		gameModes.push_back(tempString);
	}

	ReadFileToToken(data, "Players");
	// six map names so this is hard coded 
	for (int i = 0; i < 6; i++)
	{
		data >> tempString;
		players.push_back(std::stoi(tempString));
	}
	

	ConnectSocket = INVALID_SOCKET;
	clientName = "";

	command = -1;
	sendBuffer = new Buffer(512);
		receiveBuffer = new Buffer(512);
	addrinfo* result = NULL;
	addrinfo* ptr = NULL;

	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed: %d\n", iResult);
		return;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	iResult = getaddrinfo("127.0.0.1", DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return;
	}

	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET) {
			printf("socket() failed with error: %d\n", iResult);
			WSACleanup();
			return;
		}

		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}

		break;
	}

	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server");
		WSACleanup();
		return;
	}
}

Client::~Client()
{

	closesocket(ConnectSocket);
	WSACleanup();
}

/*
* Function Name : sendMessageToServer
* Return Type : void
* Parameters : NA
* Purpose : Gets the message and classifies messages
*/
void Client::sendMessageToServer()
{
	std::vector<std::string> messages;
	std::vector<std::string> multiMessages;
	std::vector<std::string> createLobbyVector;

	std::string email = "";
	std::string password = "";

	std::string mapName = ""; 
	std::string lobbyName = "";
	std::string gameMode = "";
	int maxPlayers = 0;
	std::string hostName = "";
	
	int messageLengthCount = 0;
	int totalMessageLengthCount = 0; 

	bool joinHostToLobby = false;
	

	std::string messageTS;
	do
	{
		std::cout << "Type a message : ";							//JOIN FUN
		std::getline(std::cin, input);
		const char* sendbuf = input.c_str();

		
		

		// seperate each word in the input string 
		//=========================================================
		std::istringstream iss(input);
		std::string word = "";
		while (iss >> word) {
			messages.push_back(word);
		}
		//========================================================
		// put the full commands back together 
		std::string fullCommand = "";
		for (int i = 0; i < messages.size(); i++)
		{
			int size = messages[i].size() - 1;
			
			if (messages[i][size] == ';')
			{
				char tempString[20]; 
				messages[i].copy(tempString, size, 0);
				tempString[size] = '\0';
				fullCommand += tempString;
				multiMessages.push_back(fullCommand);
				fullCommand = ""; 
			}
				
			else
				fullCommand += messages[i] + " ";
		}
		//=========================================================
		// get the total length of the messages being sent 
		for (int i = 0; i < messages.size(); i++)
		{
			if (messages[i] == "list" || messages[i] == "LIST"
				|| messages[i] == "join" || messages[i] == "JOIN"
				|| messages[i] == "leave" || messages[i] == "LEAVE"
				|| messages[i] == "register" || messages[i] == "REGISTER"
				|| messages[i] == "authenticate" || messages[i] == "AUTHENTICATE"
				|| messages[i] == "listmaps;" || messages[i] == "LISTMAPS;"
				|| messages[i] == "listgamemodes;" || messages[i] == "LISTGAMEMODES;"
				|| messages[i] == "listmaxplayers;" || messages[i] == "LISTMAXPLAYERS;")
			{
				// do nothing; 
			}
			else
			{
				int size = messages[i].size() - 1;
				if (messages[i][size] == ';')
				{
					totalMessageLengthCount += messages[i].size() - 1; 
				}
				else
				{
					totalMessageLengthCount += messages[i].size();
				}
			}
		}
		//=========================================================
		// the number of commands to be sent 
		sendBuffer->WriteInt32BE(0, multiMessages.size());
		messageLengthCount += 4;

		// the total length of all the messages being recieved 
		sendBuffer->WriteInt32BE(4, totalMessageLengthCount);
		messageLengthCount += 4;

		//=========================================================

		// for loop for sending mutiple messages if necessary 
		for (int i = 0; i < multiMessages.size(); i++)
		{
			std::string firstSendToken = getFirstTextToken(multiMessages[i]);		//JOIN 

			if (firstSendToken == "list" || firstSendToken == "LIST")
			{
				command = LIST;
				messageTS = "ls";
			}
			else if (firstSendToken == "join" || firstSendToken == "JOIN")
			{
				command = JOIN;
				messageTS = multiMessages[i].substr(multiMessages[i].find_first_of(" \t") + 1);  //FUN
			}
			else if (firstSendToken == "leave" || firstSendToken == "LEAVE")
			{
				command = LEAVE;
				messageTS = multiMessages[i].substr(multiMessages[i].find_first_of(" \t") + 1) + " " + username;  //FUN
			}
			else if (firstSendToken == "logout" || firstSendToken == "LOGOUT")
			{
				command = LOGOUT;
				messageTS = multiMessages[i].substr(multiMessages[i].find_first_of(" \t") + 1) + " " + username;  //FUN
			}
			else if (firstSendToken == "register" || firstSendToken == "REGISTER")			//register email password 
			{
				command = REGISTER;
				messageTS = multiMessages[i].substr(multiMessages[i].find_first_of(" \t") + 1);					// email password 

				//std::cout << "message first : " << messageTS << std::endl;
				//email = getFirstTextToken(messageTS);									//email
				//std::cout << "message second : " << email << std::endl;
				//password = multiMessages[i].substr(multiMessages[i].find_last_of(" \t") + 1);				//password
				//std::cout << password << std::endl;


			}
			else if (firstSendToken == "login" || firstSendToken == "LOGIN")			//authenticate email password 
			{
				command = LOGIN;
				messageTS = multiMessages[i].substr(multiMessages[i].find_first_of(" \t") + 1);					// email password 

				username = getFirstTextToken(messageTS);

				//std::cout << "message first : " << messageTS << std::endl;
				//email = getFirstTextToken(messageTS);									//email
				//std::cout << "message second : " << email << std::endl;
				//password = multiMessages[i].substr(multiMessages[i].find_last_of(" \t") + 1);				//password
				//std::cout << password << std::endl;

			}
			else if (firstSendToken == "listmaps" || firstSendToken == "LISTMAPS"
					|| firstSendToken == "listgamemodes" || firstSendToken == "LISTGAMEMODES"
					|| firstSendToken == "listmaxplayers" || firstSendToken == "LISTMAXPLAYERS")			//authenticate email password 
			{
				std::string mapNamesString = ""; 
				std::string gameModesString = "";
				std::string maxPlayersString = "";
				// ensure mapName exists 
				for (int i = 0; i < 6; i++)
				{
					mapNamesString += mapNames[i] + ", ";
					
				}// end for 

				 // ensure game mode exists 
				for (int i = 0; i < 4; i++)
				{
					gameModesString += gameModes[i] + ", ";
				}

				// ensure the proper number of max players is chosen 
				for (int i = 0; i < 6; i++)
				{
					maxPlayersString += std::to_string(players[i]) + ", ";
				}

				std::cout << "\nMap Names: " << mapNamesString << "\n" <<std::endl;
				std::cout << "Game Modes: " << gameModesString << "\n" << std::endl;
				std::cout << "Max Players: " << maxPlayersString << "\n" << std::endl;
				
			}
			else if (firstSendToken == "create" || firstSendToken == "CREATE")			//authenticate email password 
			{
				bool goodLobbyCreated = false; 

				command = CREATE;

				// get each parameter for create seperatly 
				std::istringstream iss(multiMessages[i]);
				std::string word = "";
				while (iss >> word) {
					createLobbyVector.push_back(word);
				}
				
				mapName = createLobbyVector[1];
				lobbyName = createLobbyVector[2];
				gameMode = createLobbyVector[3];
				maxPlayers = std::stoi(createLobbyVector[4]);

				// ensure mapName exists 
				for (int i = 0; i < 6; i++)
				{
					if (mapName == mapNames[i])
					{
						goodLobbyCreated = true;
						break; 
					}
					else
					{
						goodLobbyCreated = false; 
					}		
				}// end for 

				// ensure game mode exists 
				 for (int i = 0; i < 4; i++)
				 {
					 if (gameMode == gameModes[i])
					 {
						 goodLobbyCreated = true;
						 break;
					 }
					 else
					 {
						 goodLobbyCreated = false;
					 }
				 }

				 // ensure the proper number of max players is chosen 
				 for (int i = 0; i < 6; i++)
				 {
					 if (maxPlayers == players[i])
					 {
						 goodLobbyCreated = true;
						 break;
					 }
					 else
					 {
						 goodLobbyCreated = false;
					 }
				 }// end for 


				 if (goodLobbyCreated)
				 {
					 joinHostToLobby = true; 
					 messageTS += lobbyName + " " + mapName + " " + gameMode + " " + std::to_string(maxPlayers) + " " + username;
				 }


				
			}
			else
			{
				command = MESSAGE;
				messageTS = multiMessages[i];
			}

			/*sendBuffer->WriteInt32BE(0, command);
			int sizeOfMessage = messageTS.size();
			sendBuffer->WriteInt32BE(4, sizeOfMessage);
			sendBuffer->WriteString(8, messageTS);*/

			sendBuffer->WriteInt32BE(messageLengthCount, command);
			messageLengthCount += 4; 
			int sizeOfMessage = messageTS.size();
			sendBuffer->WriteInt32BE(messageLengthCount, sizeOfMessage);
			messageLengthCount += 4;
			sendBuffer->WriteString(messageLengthCount, messageTS);
			messageLengthCount += sizeOfMessage;

		}// end for 
		

		iResult = send(ConnectSocket, sendBuffer->vecBuffer.data(), sendBuffer->vecBuffer.size(), 0);
		if (iResult == SOCKET_ERROR) {
			printf("socket() failed with error: %d\n", iResult);
			closesocket(ConnectSocket);
			WSACleanup();
			return;
		}
		
		sendBuffer->clearBuffer();
		messages.clear();
		multiMessages.clear();
		messageTS = ""; 
		messageLengthCount = 0; 




		if (joinHostToLobby)
		{
			messageTS = lobbyName;
			int sizeOfMessage = messageTS.size();
			sendBuffer->WriteInt32BE(0, 1);
			messageLengthCount += 4;

			// the total length of all the messages being recieved 
			sendBuffer->WriteInt32BE(4, sizeOfMessage);
			messageLengthCount += 4;

			
			sendBuffer->WriteInt32BE(8, 1);
			
			sendBuffer->WriteInt32BE(12, sizeOfMessage);
			sendBuffer->WriteString(16, messageTS);

			iResult = send(ConnectSocket, sendBuffer->vecBuffer.data(), sendBuffer->vecBuffer.size(), 0);
			if (iResult == SOCKET_ERROR) {
				printf("socket() failed with error: %d\n", iResult);
				closesocket(ConnectSocket);
				WSACleanup();
				return;
			}

			sendBuffer->clearBuffer();
			messages.clear();
			multiMessages.clear();
			messageTS = "";
			messageLengthCount = 0;

			joinHostToLobby = false;

		}

	} while (1);

}

/*
* Function Name : getFirstTextToken
* Return Type : string
* Parameters :  string
* Purpose : Gets first word from the provided message
*/
std::string Client::getFirstTextToken(std::string clientMessage)
{
	std::string token = clientMessage.substr(0, clientMessage.find(" "));
	return token;
}



/*
* Function Name : receiveMessages
* Return Type : void
* Parameters :  NA
* Purpose : Receives messages from server
*/
void Client::receiveMessages()
{
	bool recvMessage = true;
	bool connected = true;
	do
	{
		iResult = recv(ConnectSocket, receiveBuffer->vecBuffer.data(), receiveBuffer->vecBuffer.size(), 0);
		if (iResult == SOCKET_ERROR) 
		{
			printf("***Server disconnected!***\n");
			closesocket(ConnectSocket);
		}

		if (iResult > 0) 
		{

			int typeOfProtocol = receiveBuffer->ReadInt32BE(0);
			int numOfBytesToRecv = receiveBuffer->ReadInt32BE(4);
			std::string messageRecv = receiveBuffer->ReadString(8, numOfBytesToRecv);

			std::cout << messageRecv << std::endl; 
		}

	} while (recvMessage && connected);
}

void Client::ReadFileToToken(std::ifstream &file, std::string token)
{
	bool bKeepReading = true;
	std::string garbage;
	do
	{
		file >> garbage;		// Title_End??
		if (garbage == token)
		{
			return;
		}
	} while (bKeepReading);
	return;
}