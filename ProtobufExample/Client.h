#ifndef _Client_HG_
#define _Client_HG_


#define UNICODE
#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <WinSock2.h>
#include <ws2tcpip.h>
#include <vector>
#include <fstream>

#include "..\shared\Buffer.h"

#include <iostream>
#include <sstream>

#pragma comment (lib, "Ws2_32.lib")

#define DEFAULT_PORT "5000"
#define DEFAULT_BUFFER_LENGTH 512

enum eType
{
	LIST,
	JOIN,
	LEAVE,
	MESSAGE,
	REGISTER,
	LOGIN,
	CREATE,
	LOGOUT,
};



class Client
{
public:
	Client();
	~Client();

	WSADATA wsaData;
	SOCKET ConnectSocket = INVALID_SOCKET;
	char recvbuf[DEFAULT_BUFFER_LENGTH];
	struct addrinfo* result = NULL;
	struct addrinfo* ptr = NULL;
	struct addrinfo hints;

	std::vector<std::string> mapNames;
	std::vector<std::string> gameModes;
	std::vector<int> players;

	std::string filePathForTXTLoader;
	std::string username;
	
	int iResult;

	int command;

	Buffer* sendBuffer;
	Buffer* receiveBuffer;

	std::string clientName;
	std::string input;
	void sendMessageToServer();									//Sends message to a server by assigning a protocol

	void receiveMessages();										//Receives broadcast messages from the server
	int requestid = 7;
	std::string getFirstTextToken(std::string clientMessage);		//Get first string to map any command

	void ReadFileToToken(std::ifstream &file, std::string token);

	//Web::CreateAccountWeb createAccount;
	std::string serializeClientInfo;



private:

};

#endif // !_Client_HG_
