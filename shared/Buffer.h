#ifndef BUFFER_HG
#define BUFFER_HG

#include <vector> 
#include <string>
//same
class Buffer {
public:
	Buffer(size_t size);

	void WriteInt32LE(size_t index, int value);			//Serialize int in LITTLE ENDIAN
	void WriteInt32BE(size_t index, int value);			//Serialize int in BIG ENDIAN
	void WriteShort16BE(size_t index, int value);		//Serialize short in BIG ENDIAN

	int ReadInt32LE(size_t index);						//Deserialize int in LITTLE ENDIAN
	int ReadInt32BE(size_t index);						//Deserialize int in BIG ENDIAN
	short ReadShort16BE(size_t index);					//Deserialize short in BIG ENDIAN


	void WriteString(size_t index, std::string value);	//Serialize string
	std::string ReadString(size_t index, int sizeOfReceivedMessage);	//Deserialize string
	void clearBuffer();


	std::vector<char>vecBuffer;

	

	int mReadIndex;
	int mWriteIndex;
};


#endif 