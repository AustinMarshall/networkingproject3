#pragma comment(lib, "Ws2_32.lib")
#include "Server.h"

mySocket mostRecentTempSocket;

Server::Server()
{

	size = 512;
	index = 0;
	recvBuffer = new Buffer(size);
	sendMessages = true;
	recvbuf[DEFAULT_BUFFER_LENGTH];
	recvbuflen = DEFAULT_BUFFER_LENGTH;
	result = 0;
	requestid = 0;
	sendBufferMiddleware = new Buffer(512);
	receiveBufferMiddleware = new Buffer(512);

	clientName = "";

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;




	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed: %d\n", iResult);
		return;
	}

	// Socket()
	ListenSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (ListenSocket == INVALID_SOCKET) {
		printf("socket() failed with error %d\n", WSAGetLastError());
		WSACleanup();
		return;
	}

	// Bind()
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind() failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		return;
	}

	// Listen()
	if (listen(ListenSocket, 5)) {
		printf("listen() failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return;
	}
	addrinfo* ptr = NULL;

	ZeroMemory(&hintsAuth, sizeof(hintsAuth));
	hintsAuth.ai_family = AF_UNSPEC;
	hintsAuth.ai_socktype = SOCK_STREAM;
	hintsAuth.ai_protocol = IPPROTO_TCP;

	iResultMiddleware = getaddrinfo("127.0.0.1", DEFAULT_AUTHENTICATION_PORT, &hintsAuth, &resultAuth);
	if (iResultMiddleware != 0) {
		printf("getaddrinfo failed with error: %d\n", iResultMiddleware);
		WSACleanup();
		return;
	}

	for (ptr = resultAuth; ptr != NULL; ptr = ptr->ai_next) {
		AuthenticateSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (AuthenticateSocket == INVALID_SOCKET) {
			printf("socket() failed with error: %d\n", iResultMiddleware);
			WSACleanup();
			return;
		}

		iResultMiddleware = connect(AuthenticateSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResultMiddleware == SOCKET_ERROR) {
			closesocket(AuthenticateSocket);
			AuthenticateSocket = INVALID_SOCKET;
			continue;
		}

		break;
	}

	freeaddrinfo(resultAuth);

	if (AuthenticateSocket == INVALID_SOCKET) {
		printf("Unable to connect to server");
		WSACleanup();
		return;
	}
	return;
}

Server::~Server()
{
	for (int index = 0; index != threads.size(); index++)
	{
		this->threads[index]->join();
	}

	closesocket(ListenSocket);
	WSACleanup();
	return;
}

/*
* Function Name : acceptClients
* Return Type : void
* Parameters : NA
* Purpose : Accepts new clients assigning a new thread for each client
*/
void Server::acceptClients()
{
	int clientCounter = 0;
	mySocket tempSocket;

	do {
		SOCKET tempClientSocket;
		// Accept()
		tempClientSocket = accept(ListenSocket, NULL, NULL);
		if (tempClientSocket == INVALID_SOCKET) {
			printf("accept() failed with error: %d\n", WSAGetLastError());
			closesocket(ListenSocket);
			WSACleanup();
			//return;
		}

		tempSocket.clientSocket = tempClientSocket;
		tempSocket.clientName = "client " + std::to_string(clientCounter);
		tempSocket.id = clientCounter;
		clientCounter++;

		//this->clientSockets.push_back(tempClientSocket);		
		this->clientSockets.push_back(tempSocket);
		std::thread* clientListenThread = (new std::thread(&Server::listenToClient, this, tempSocket));

		this->threads.push_back(clientListenThread);
		std::cout << "Client " << tempSocket.clientName <<" Connected - New Thread Started" << std::endl; 
	} while (sendMessages);

}// end acceptClients


 /*
 * Function Name : listenToClient
 * Return Type : void
 * Parameters : mySocket (struct containing socket, clientname and client id)
 * Purpose : Listens to available clients, receives messages from the clien
			 Perfoms necessary actions upon clients' request
 */
void Server::listenToClient(mySocket tempSocket)
{


	mostRecentTempSocket = tempSocket;

	bool connected = true;
	
	

	do {

		iResult = recv(tempSocket.clientSocket, recvBuffer->vecBuffer.data(), recvBuffer->vecBuffer.size(), 0);
		//std::cout << "Server Socked : " << tempSocket.clientSocket;
		if (iResult == SOCKET_ERROR) {
			std::cout << "\nClient " << tempSocket.clientName << " has exited the application" << std::endl;
			closesocket(tempSocket.clientSocket);

			for (int i = 0; i != clientSockets.size(); i++)
			{
				if (clientSockets[i].clientSocket == tempSocket.clientSocket)
				{
					this->clientSockets.erase(clientSockets.begin() + i);
					connected = false;
					break;
				}
			}

		}

		if (iResult > 0) {

			int numberOfCommands = recvBuffer->ReadInt32BE(0);
			int totalLengthOfmessages = recvBuffer->ReadInt32BE(4);
			int totalLengthOfmessagesRecv = 0;

			int numberOfBytesRead = 8; 

			for (int i = 0; i < numberOfCommands; i++)
			{
				int typeOfProtocol = recvBuffer->ReadInt32BE(numberOfBytesRead);
				numberOfBytesRead += 4; 
				int numOfBytesToRecv = recvBuffer->ReadInt32BE(numberOfBytesRead);
				numberOfBytesRead += 4;
				std::string receivedMessage = recvBuffer->ReadString(numberOfBytesRead, numOfBytesToRecv);
				numberOfBytesRead += numOfBytesToRecv;
				totalLengthOfmessagesRecv += numOfBytesToRecv; 

				if (typeOfProtocol == 0)
				{
					listAvailableLobbies(tempSocket);
				}
				else if (typeOfProtocol == 1)
				{
					tempSocket.associatedLobbies.push_back(receivedMessage);
					std::cout << tempSocket.associatedLobbies[0];
					assignToLobby(receivedMessage, tempSocket);
				}
				else if (typeOfProtocol == 2)
				{

					leaveFromLobby(tempSocket, receivedMessage);
				}
				else if (typeOfProtocol == 3)
				{
					broadcastMessage(tempSocket, receivedMessage);
				}
				else if (typeOfProtocol == 4 || typeOfProtocol == 5)
				{
					//Register or authenticate user depending on the type of protocol
					sendMessageToAuthServer(receivedMessage, typeOfProtocol);

				}
				else if (typeOfProtocol == 6)
				{
					//create a lobby
					std::vector<std::string> createLobbyVector;

					std::istringstream iss(receivedMessage);
					std::string word = "";
					while (iss >> word) {
						createLobbyVector.push_back(word);
					}

					createLobby(createLobbyVector[1], createLobbyVector[0], createLobbyVector[2], createLobbyVector[3], createLobbyVector[4]);

				}
				else if (typeOfProtocol == 7)
				{
					
					//logout(tempSocket, receivedMessage);
					sendMessageToAuthServer(receivedMessage, typeOfProtocol);
				}
			}// end for 


		

			 // checking to see if all of the message was received
			if (totalLengthOfmessagesRecv == totalLengthOfmessages)
			{
				std::cout << "\nFull Message received!" << std::endl;
			}
			else
			{
				std::cout << "\nFull Message *NOT* received!" << std::endl;
			}


		}

		

	} while (sendMessages && connected);

	


}// end listenToClient


 /*
 * Function Name : getFirstTextToken
 * Return Type : string
 * Parameters :  string
 * Purpose : Gets first word from the provided message
 */
std::string Server::getFirstTextToken(std::string serverMessage)
{
	std::string token = serverMessage.substr(0, serverMessage.find(" "));
	return token;
}

/*
* Function Name : assignToLobby
* Return Type : void
* Parameters : mySocket (struct containing socket, clientname and client id), string
* Purpose : Assign a client into a suggested lobby upon request
*/
void Server::assignToLobby(std::string lobbyName, mySocket tempSocket)
{

	mostRecentTempSocket = tempSocket;


	Buffer tempBuffer(DEFAULT_BUFFER_LENGTH);
	//======================================================================================================================================
	// constructing the join lobby strings 
	std::string broadcastMessage = "";
	std::string broadcastMessageTOSELF = "";

	broadcastMessage = "**" + tempSocket.clientName + " has joined the lobby " + tempSocket.associatedLobbies.data()->c_str() + "**";

	broadcastMessageTOSELF += "**You have joined the lobby/s "; // add asterisks and message 
	broadcastMessage = "**" + tempSocket.clientName + " has joined the lobby/s "; 
	for (int messageIndex = 0; messageIndex != tempSocket.associatedLobbies.size(); messageIndex++)
	{
		if (tempSocket.associatedLobbies.size() == 1)
		{
			broadcastMessageTOSELF += tempSocket.associatedLobbies[messageIndex].data();
			broadcastMessage += tempSocket.associatedLobbies[messageIndex].data();
		}
		else
		{
			broadcastMessageTOSELF += tempSocket.associatedLobbies[messageIndex].data();
			broadcastMessage += tempSocket.associatedLobbies[messageIndex].data();
			if (messageIndex + 1 != tempSocket.associatedLobbies.size())
			{
				broadcastMessageTOSELF += " and ";
				broadcastMessage += " and ";
			}
				
		}
	}//end for 
	broadcastMessageTOSELF += "**";// add asterisks to end of message 
	//===============================================================================================================================================
	//TODO
	// sending the join message to everyone in the current lobby and yourself 

	for (int index = 0; index != clientSockets.size(); index++)
	{
		if (clientSockets[index].clientName == tempSocket.clientName)
		{
			clientSockets[index].associatedLobbies.push_back(lobbyName);
		}// end if 
		for (int lobbyIndex = 0; lobbyIndex !=clientSockets[index].associatedLobbies.size(); lobbyIndex++)
		{

			if (clientSockets[index].associatedLobbies[lobbyIndex] == lobbyName)
			{
				tempBuffer.mWriteIndex = 0;
				if (clientSockets[index].clientSocket == tempSocket.clientSocket)
				{
					tempBuffer.WriteInt32BE(4, broadcastMessageTOSELF.size()); // send message to yourself that you have joined the lobby 
					tempBuffer.WriteString(8, broadcastMessageTOSELF); // send message to yourself that you have joined the lobby 
				}
				else
				{
					tempBuffer.WriteInt32BE(4, broadcastMessage.size()); // send message to yourself that you have joined the lobby 
					tempBuffer.WriteString(8, broadcastMessage);// send message to everyone in the lobby that you have joined 
				}
				iResult = send(clientSockets[index].clientSocket, tempBuffer.vecBuffer.data(), tempBuffer.vecBuffer.size(), 0);
				if (iResult == SOCKET_ERROR) {
					printf("recv failed with error: %d\n", WSAGetLastError());
					closesocket(clientSockets[index].clientSocket);
					WSACleanup();

				}// end if 
			}// end if 
		}// end nested for 
			
	}// end for
	

	int breakpoint = 0; 
	
}

/*
* Function Name : broadcastMessage
* Return Type : void
* Parameters : mySocket (struct containing socket, clientname and client id), string
* Purpose : Displays messages to clients in the same lobby.
*/
void Server::broadcastMessage(mySocket activeClient, std::string receivedMessage)
{
	Buffer tempBuffer(DEFAULT_BUFFER_LENGTH);
	std::vector<std::string> clientsActiveLobbies;

	for (int index = 0; index != activeClient.associatedLobbies.size(); index++)
	{
		clientsActiveLobbies.push_back(activeClient.associatedLobbies[index]);
	}// end for 



	for (int index = 0; index != clientSockets.size(); index++)
	{
		if (clientSockets[index].clientSocket == activeClient.clientSocket)
		{
			continue;
		}

		for (int lobbyIndex = 0; lobbyIndex != clientsActiveLobbies.size(); lobbyIndex++)
		{
			if (clientSockets[index].associatedLobbies.size() == 0)
			{
				continue;
			}

			if (clientSockets[index].associatedLobbies[lobbyIndex] == clientsActiveLobbies[lobbyIndex])
			{
				tempBuffer.WriteInt32BE(4, receivedMessage.size()); // send message to yourself that you have joined the lobby 
				tempBuffer.WriteString(8, receivedMessage); // send message to yourself that you have joined the lobby 

				iResult = send(clientSockets[index].clientSocket, tempBuffer.vecBuffer.data(), tempBuffer.vecBuffer.size(), 0);
				if (iResult == SOCKET_ERROR) {
					printf("recv failed with error: %d\n", WSAGetLastError());
					closesocket(clientSockets[index].clientSocket);
					WSACleanup();

				}// end if 
			}// end if 
		}// end nested for  
	}// end for 



}// end broadcastMessage

 /*
 * Function Name : leaveFromLobby
 * Return Type : void
 * Parameters : mySocket (struct containing socket, clientname and client id), string
 * Purpose : Disconnects a client from a lobby upon request
 */
void Server::leaveFromLobby(mySocket activeClient, std::string receivedMessage)
{

	

	Buffer tempBuffer(DEFAULT_BUFFER_LENGTH);
	std::string clientActiveLobby = activeClient.associatedLobbies[0];

	std::string leaveMessage =  " you have left the lobby - " + clientActiveLobby;
	std::vector<std::string> clientsActiveLobbies;
	std::vector<std::string> lobbyVector;
	std::vector<std::string> receivedMessageVector;
	

	std::istringstream iss(receivedMessage);
	std::string word = "";
	while (iss >> word) {
		receivedMessageVector.push_back(word);
	}
	

	activeClient.associatedLobbies.clear();

	tempBuffer.WriteInt32BE(4, leaveMessage.size()); // send message to yourself that you have joined the lobby 
	tempBuffer.WriteString(8, leaveMessage); // send message to yourself that you have joined the lobby 

	iResult = send(clientSockets[index].clientSocket, tempBuffer.vecBuffer.data(), tempBuffer.vecBuffer.size(), 0);

	if (iResult == SOCKET_ERROR) {
		printf("recv failed with error: %d\n", WSAGetLastError());
		closesocket(clientSockets[index].clientSocket);
		WSACleanup();
	}


	for (int index = 0; index != gameLobby.size(); index++)
	{
		// get each parameter for the lobby name 
		std::istringstream iss(gameLobby[index].data());
		std::string word = "";
		while (iss >> word) {
			lobbyVector.push_back(word);
		}



		if (receivedMessageVector[1] == lobbyVector[13])
		{
			// host left the lobby kick everyone else out too 
			
				for (int j = 0; j != clientSockets.size(); j++)
				{
					if (clientSockets[j].associatedLobbies[0] == clientActiveLobby)
					{
						clientSockets[j].associatedLobbies.clear();

						leaveMessage = " The host has left the lobby! you've been disconnected! - ";

						tempBuffer.WriteInt32BE(4, leaveMessage.size()); // send message to yourself that you have joined the lobby 
						tempBuffer.WriteString(8, leaveMessage); // send message to yourself that you have joined the lobby 

						iResult = send(clientSockets[j].clientSocket, tempBuffer.vecBuffer.data(), tempBuffer.vecBuffer.size(), 0);

						if (iResult == SOCKET_ERROR) {
							printf("recv failed with error: %d\n", WSAGetLastError());
							closesocket(clientSockets[index].clientSocket);
							WSACleanup();
						}
					}
				}

				
		}


	}





	//for (int index = 0; index != activeClient.associatedLobbies.size(); index++)
	//{
	//	clientsActiveLobbies.push_back(activeClient.associatedLobbies[index]);
	//}// end for 



	//for (int index = 0; index != clientSockets.size(); index++)
	//{
	//	if (clientSockets[index].clientSocket == activeClient.clientSocket)
	//	{
	//		continue;
	//	}

	//	for (int lobbyIndex = 0; lobbyIndex != clientsActiveLobbies.size(); lobbyIndex++)
	//	{
	//		if (clientSockets[index].associatedLobbies.size() == 0)
	//		{
	//			continue;
	//		}

	//		

	//		if (clientSockets[index].associatedLobbies[lobbyIndex] == clientsActiveLobbies[lobbyIndex])
	//		{
	//			tempBuffer.WriteInt32BE(4, leaveMessage.size()); // send message to yourself that you have joined the lobby 
	//			tempBuffer.WriteString(8, leaveMessage); // send message to yourself that you have joined the lobby 

	//			iResult = send(clientSockets[index].clientSocket, tempBuffer.vecBuffer.data(), tempBuffer.vecBuffer.size(), 0);
	//			if (iResult == SOCKET_ERROR) {
	//				printf("recv failed with error: %d\n", WSAGetLastError());
	//				closesocket(clientSockets[index].clientSocket);
	//				WSACleanup();

	//			}// end if 
	//		}// end if 
	//	}// end nested for  
	//}// end for 


	//for (int index = 0; index != clientSockets.size(); index++)
	//{
	//	if (clientSockets[index].clientSocket == activeClient.clientSocket)
	//	{
	//		for (int lobbyIndex = 0; lobbyIndex != clientsActiveLobbies.size(); lobbyIndex++)
	//		{
	//			if (lobbyName == clientsActiveLobbies[lobbyIndex])
	//			{
	//				clientSockets[index].associatedLobbies.erase(clientSockets[index].associatedLobbies.begin() + lobbyIndex);

	//			}// end if 
	//		}// end for 
	//	}
	//	
	//}//end for 
	
	


}

/*
* Function Name : listAvailableLobbies
* Return Type : void
* Parameters : mySocket (struct containing socket, clientname and client id)
* Purpose : Lists all the Lobbies available to join to the clients
*/
void Server::listAvailableLobbies(mySocket tempSocket)
{
	mostRecentTempSocket = tempSocket;


	Buffer tempBuffer(DEFAULT_BUFFER_LENGTH);
	std::string lobbies="\n\nAvailable lobbies:\n";
	//TODO

	std::vector<std::string> lobbyVector; 
	std::vector<std::string> LobbyName; 
	std::vector<int> numberOfPlayersInLobby;

	// get all the lobbies that all clients are a part of 
	for (int i = 0; i < clientSockets.size(); i++)
	{
		if (clientSockets[i].associatedLobbies.size() == 0)
		{
			continue;
		}

		if (LobbyName.size() == 0)
		{
			LobbyName.push_back(clientSockets[i].associatedLobbies[0]);
			numberOfPlayersInLobby.push_back(1);
			continue;
		}
			

		for (int j = 0; j < LobbyName.size(); j++)
		{
			if (clientSockets[i].associatedLobbies[0] == LobbyName[j])
			{
				numberOfPlayersInLobby[j]++;
			}
			else
			{
				LobbyName.push_back(clientSockets[i].associatedLobbies[0]);
				numberOfPlayersInLobby.push_back(1);
				break;
			}
			
		}
	}
	
	std::string lobbyNameReconstructed = "\nAvailable Lobbies:\n";
	int numberOfPlayersInThisLobby = 0;

	for (int index = 0; index != gameLobby.size(); index++)
	{
		// get each parameter for the lobby name 
		std::istringstream iss(gameLobby[index].data());
		std::string word = "";
		while (iss >> word) {
			lobbyVector.push_back(word);
		}
		
		


		// get the amount of players in the current lobby
		for (int numPlayersIndex = 0; numPlayersIndex < LobbyName.size(); numPlayersIndex++)
		{
			std::string lobbyNamewithComma = LobbyName[numPlayersIndex] + ",";
			if (lobbyNamewithComma == lobbyVector[2])
			{
				numberOfPlayersInThisLobby = numberOfPlayersInLobby[index];
			}
		}
		

		// reconstruct the lobby name 
		for (int j = 0; j < lobbyVector.size(); j++)
		{
			

			if (j == 9)
			{
				lobbyNameReconstructed += lobbyVector[j] + " ";
				lobbyNameReconstructed += std::to_string(numberOfPlayersInThisLobby) + "|";
			}
			else
			{
				lobbyNameReconstructed += lobbyVector[j] + " ";
			}
			
		}


		//std::cout << lobbyNameReconstructed << std::endl;
		/*lobbies += lobbyNameReconstructed + "\n";*/

		lobbyNameReconstructed += "\n";
		tempBuffer.WriteInt32BE(4, lobbyNameReconstructed.size());					 // send message to yourself that you have joined the lobby 
		tempBuffer.WriteString(8, lobbyNameReconstructed);		 // send message to yourself that you have joined the lobby 


		iResult = send(tempSocket.clientSocket, tempBuffer.vecBuffer.data(), tempBuffer.vecBuffer.size(), 0);
		int breakpoint = 0;
		if (iResult == SOCKET_ERROR)
		{
			printf("recv failed with error: %d\n", WSAGetLastError());
			closesocket(tempSocket.clientSocket);
			WSACleanup();
		}



		lobbyNameReconstructed = ""; 
		lobbyVector.clear();

	}

	
}

///*
//* Function Name : addLobby
//* Return Type : void
//* Parameters : NA
//* Purpose : Add Initial Lobbies to vector of chat Lobby
//*/
//void Server::addLobby()
//{
//
//	
//	gameLobby.push_back("Lobby1");
//	gameLobby.push_back("Lobby2");
//	gameLobby.push_back("Lobby3");
//}


/*
* Function Name : receiveMessagesFromAuth
* Return Type : void
* Parameters : NA
* Purpose : Reveive messages sent from Authentication Server,
			Also Forwards the received messages to the client
			Acts as a medium for Authenticatin Server and the Chat client
*/
void Server::receiveMessagesFromAuth()
{
	bool recvMessage = true;
	bool connected = true;

	int loopCounter = 0;
	std::string messageRecv;
	//Web::CreateAccountWeb deserialized_createAccount;

	do
	{

		char getMessageFromAuthServer[512];
		std::string test = "";
		iResultMiddleware = recv(AuthenticateSocket, receiveBufferMiddleware->vecBuffer.data(), receiveBufferMiddleware->vecBuffer.size(), 0);
		if (iResultMiddleware == SOCKET_ERROR) {
			std::cout << "\nServer " << AuthenticateSocket << " has exited the application" << std::endl;
			closesocket(AuthenticateSocket);

		}
		if (iResultMiddleware  > 0) {

			int typeOfProtocol = receiveBufferMiddleware->ReadInt32BE(0);
			int numOfBytesToRecv = receiveBufferMiddleware->ReadInt32BE(4);
			messageRecv = receiveBufferMiddleware->ReadString(8, numOfBytesToRecv);

		}

		std::cout << messageRecv << std::endl;

		

		//broadcastMessage(mostRecentTempSocket, messageRecv);
		Buffer tempBuffer(DEFAULT_BUFFER_LENGTH);
		tempBuffer.WriteInt32BE(4, messageRecv.size());
		tempBuffer.WriteString(8, messageRecv);

		iResult = send(mostRecentTempSocket.clientSocket, tempBuffer.vecBuffer.data(), tempBuffer.vecBuffer.size(), 0);
		
		if (iResult == SOCKET_ERROR) {
			printf("recv failed with error: %d\n", WSAGetLastError());
			closesocket(mostRecentTempSocket.clientSocket);
			WSACleanup();

		}// end if 
		loopCounter++;
		/*if (loopCounter > 5)
		break; */

	} while (recvMessage && connected);
}


/*
* Function Name : sendMessageToAuthServer
* Return Type : void
* Parameters : string, int
* Purpose : Serializes message received from Chat Client, 
			send the message to authentication server for futher processing
*/

void Server::sendMessageToAuthServer(std::string messageTS, int command)							
{

	sendBufferMiddleware->WriteInt32BE(0, command);
	int sizeOfMessage = messageTS.size();
	sendBufferMiddleware->WriteInt32BE(4, sizeOfMessage);

	sendBufferMiddleware->WriteString(8, messageTS);
	int breakpoint = 0;																	//Incrementing request id upon every client request

	iResult = send(AuthenticateSocket, sendBufferMiddleware->vecBuffer.data(), sendBufferMiddleware->vecBuffer.size(), 0);
	if (iResult == SOCKET_ERROR) {
		printf("socket() failed with error: %d\n", iResult);
		closesocket(AuthenticateSocket);
		WSACleanup();
		return;
	}

	
}


/*
* Function Name : createLobby
* Return Type : void
* Parameters : string, string, int, string
* Purpose : creates a new game lobby 
*/
void Server::createLobby(std::string mapName, std::string lobbyName, std::string gameMode , std::string maxPlayers, std::string hostName)
{

	bool goodLobbyName = true; 

	for (int i = 0; i != gameLobby.size(); i++)
	{

		//create a lobby
		std::vector<std::string> createLobbyVector;

		std::istringstream iss(gameLobby[i]);
		std::string word = "";
		while (iss >> word) {
			createLobbyVector.push_back(word);
		}

		std::string lobbyNameWithComma = lobbyName + ",";

		if (lobbyNameWithComma == createLobbyVector[2])
		{
			goodLobbyName = false; 
		}
	}
	
	if(goodLobbyName == true)
		gameLobby.push_back("Lobby Name: " + lobbyName + ", map Name: " + mapName + ", Game Mode: " + gameMode + ", Players: " + maxPlayers + ", Host Name: " + hostName);
	else
	{
		std::string sendMsg = "A lobby with this name already exists, You will be put into the already existing lobby!";

		Buffer tempBuffer(DEFAULT_BUFFER_LENGTH);
		tempBuffer.WriteInt32BE(4, sendMsg.size());
		tempBuffer.WriteString(8, sendMsg);

		iResult = send(mostRecentTempSocket.clientSocket, tempBuffer.vecBuffer.data(), tempBuffer.vecBuffer.size(), 0);

		if (iResult == SOCKET_ERROR) {
			printf("recv failed with error: %d\n", WSAGetLastError());
			closesocket(mostRecentTempSocket.clientSocket);
			WSACleanup();

		}// end if 
	}

	int breakpoint = 0; 
}



/*
* Function Name : logout
* Return Type : void
* Parameters : string, 
* Purpose : logs out the user who is currently logged into the client 
*/
void Server::logout(mySocket activeClient, std::string username)
{
	std::vector<std::string> receivedMessageVector;


	std::istringstream iss(username);
	std::string word = "";
	while (iss >> word) {
		receivedMessageVector.push_back(word);
	}
}