#define UNICODE
#define WIN32_LEAN_AND_MEAN
#include <vector>
#include<thread>
#include "Server.h"
//#include "accountweb.pb.h"


std::string exitString = ""; 

int main(int argc, char** argv) {
	if (argc != 2) {
		printf("usage: %s server-name\n", argv[0]);
		return 1;
	}
	
	Server server; 
	
	//thread for authenication server 
	std::thread* listenthreadAuth = (new std::thread(&Server::receiveMessagesFromAuth, server));
	std::cout << "Listen Thread at Chat Server to Authentication Server : " << listenthreadAuth->get_id() << std::endl;


	//server.addLobby();
	//thread for client
	std::thread* listenthreadAccept = (new std::thread(&Server::acceptClients, server));
	std::cout << "Listen Thread at Chat Server to Chat Client : " << listenthreadAccept->get_id() << std::endl;


	std::cout << "type exit to leave server" << std::endl;


	while (server.sendMessages)
	{
		std::cin >> exitString;
		std::cout << "type exit to leave server" << std::endl;

		if (exitString == "exit")
			server.sendMessages = false;
	}
		
	//Web::CreateAccountWeb serverCreateAccount;
	
	system("pause");

	
}