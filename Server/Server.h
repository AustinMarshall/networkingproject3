#ifndef _Server_HG_ 
#define _Server_HG_ 

#define UNICODE
#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <WinSock2.h>
#include <WS2tcpip.h>

#include "..\shared\Buffer.h"


#include <iostream>
#include <thread>
#include <sstream>
#include <map>



#define DEFAULT_AUTHENTICATION_PORT "4500"
#define DEFAULT_PORT "5000"
#define DEFAULT_BUFFER_LENGTH 512



enum action
{
	JOIN = 0,
	LEAVE = 1,
	SEND = 2,
	RECEIVE = 3,
	LISTROOMS = 4
};


struct mySocket
{
	SOCKET clientSocket;
	std::string clientName;
	std::vector<std::string>associatedLobbies;
	int id;
};



class Server
{
public:

	size_t size;
	size_t index;

	Buffer* recvBuffer;
	
	bool sendMessages;
	std::vector<mySocket> clientSockets;
	std::vector<std::thread*> threads;
	std::vector<std::string> gameLobby;

	std::string clientName;

	WSADATA wsaData;
	int iResult;

	char recvbuf[DEFAULT_BUFFER_LENGTH];
	int recvbuflen = DEFAULT_BUFFER_LENGTH;

	SOCKET ListenSocket;
	
	struct addrinfo* result;
	struct addrinfo hints;

	struct addrinfo* resultAuth;
	struct addrinfo hintsAuth;

	int requestid;

	Server();
	~Server(); 

	int iResultMiddleware;
	int commandClient;
	Buffer* sendBufferMiddleware;
	Buffer* receiveBufferMiddleware;
	SOCKET AuthenticateSocket = INVALID_SOCKET;
	
	void Server::sendMessageToAuthServer(std::string messageTS, int command);									//Sends message to a server by assigning a protocol

	void receiveMessagesFromAuth();												//Receive Serialized message from authentication server and pass message to client


	void acceptClients();														//Accepts Clients on a thread and creates a new thread to listen

	void listenToClient(mySocket tempSocket);									//Keeps on listening for any incoming client

	void assignToLobby(std::string lobbyName, mySocket tempSocket);				//Join a lobby

	void broadcastMessage(mySocket activeClient, std::string receivedMessage);	// broadcast a message to everyone in the current lobby 

	void leaveFromLobby(mySocket activeClient, std::string lobbyName);			//Leave a lobby

	void listAvailableLobbies(mySocket tempSocket);								//Lists all available lobbies upon query of the client

	void logout(mySocket activeClient, std::string username);					// logs out the user currently logged into the client 

	//void addLobby();															//Add lobbies to a vector of lobbies
	
	std::string getFirstTextToken(std::string serverMessage);					//Gets first word from input string

	void Server::createLobby(std::string mapName, std::string lobbyName, std::string gameMode, std::string maxPlayers, std::string hostName); // create a new lobby for players to join 

};

#endif